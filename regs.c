/* regs.c - architected registers state routines */

/* SimpleScalar(TM) Tool Suite
 * Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
 * All Rights Reserved. 
 * 
 * THIS IS A LEGAL DOCUMENT, BY USING SIMPLESCALAR,
 * YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.
 * 
 * No portion of this work may be used by any commercial entity, or for any
 * commercial purpose, without the prior, written permission of SimpleScalar,
 * LLC (info@simplescalar.com). Nonprofit and noncommercial use is permitted
 * as described below.
 * 
 * 1. SimpleScalar is provided AS IS, with no warranty of any kind, express
 * or implied. The user of the program accepts full responsibility for the
 * application of the program and the use of any results.
 * 
 * 2. Nonprofit and noncommercial use is encouraged. SimpleScalar may be
 * downloaded, compiled, executed, copied, and modified solely for nonprofit,
 * educational, noncommercial research, and noncommercial scholarship
 * purposes provided that this notice in its entirety accompanies all copies.
 * Copies of the modified software can be delivered to persons who use it
 * solely for nonprofit, educational, noncommercial research, and
 * noncommercial scholarship purposes provided that this notice in its
 * entirety accompanies all copies.
 * 
 * 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
 * PROHIBITED WITHOUT A LICENSE FROM SIMPLESCALAR, LLC (info@simplescalar.com).
 * 
 * 4. No nonprofit user may place any restrictions on the use of this software,
 * including as modified by the user, by any other authorized user.
 * 
 * 5. Noncommercial and nonprofit users may distribute copies of SimpleScalar
 * in compiled or executable form as set forth in Section 2, provided that
 * either: (A) it is accompanied by the corresponding machine-readable source
 * code, or (B) it is accompanied by a written offer, with no time limit, to
 * give anyone a machine-readable copy of the corresponding source code in
 * return for reimbursement of the cost of distribution. This written offer
 * must permit verbatim duplication by anyone, or (C) it is distributed by
 * someone who received only the executable form, and is accompanied by a
 * copy of the written offer of source code.
 * 
 * 6. SimpleScalar was developed by Todd M. Austin, Ph.D. The tool suite is
 * currently maintained by SimpleScalar LLC (info@simplescalar.com). US Mail:
 * 2395 Timbercrest Court, Ann Arbor, MI 48105.
 * 
 * Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "host.h"
#include "misc.h"
#include "machine.h"
#include "loader.h"
#include "regs.h"

// Julian Pavon Rivera ************************************************************************************
static struct register_alias_table RAT_t;
static struct shadow_reg_table *SRT_t;
// Julian Pavon Rivera ************************************************************************************

/* create a register file */
struct regs_t *
regs_create(void)
{
  struct regs_t *regs;

  regs = calloc(1, sizeof(struct regs_t));
  if (!regs)
    fatal("out of virtual memory");

  return regs;
}

/* initialize architected register state */
void
regs_init(struct regs_t *regs)		/* register file to initialize */
{
  /* FIXME: assuming all entries should be zero... */
  memset(regs, 0, sizeof(*regs));

  /* regs->regs_R[MD_SP_INDEX] and regs->regs_PC initialized by loader... */
}

// Julián Pavón Rivera *************************************************************************************
void alias_init()
{
  memset(&RAT_t, 0, sizeof(RAT_t));
  //printf("Register alias Table Creada\n");
}

void FRL_init()
{
  FRLFree = FRLUsed = 0;
  FRL_FP_Free = FRL_FP_Used = 0;
  for(int i=0; i<(FRL_SIZE+FRL_FP_SIZE); i++)
  {
    FRL[i] = (i+1);
  }
}

void SRT_create(int fetch_queue_size, int RUU_size)
{
  SRTIn = 0;
  SRTSize = RUU_size/fetch_queue_size;
  //printf("SRT size : %u\n", SRTSize);
  SRT_t = calloc(SRTSize, sizeof(struct shadow_reg_table));
  if (!SRT_t)
    fatal("out of virtual memory");
  //printf("Shadow Register Table Creada\n");
}

int register_renaming(int *in1, int *in2, int *in3, int *out1, int *out2)
{
  int answer = 0;
  //printf("Input dependencies: %d\t %d\t %d\n", *in1, *in2, *in3);
  //printf("Output dependencies: %d\t %d\n", *out1, *out2);
  int in_1, in_2, in_3, out_1 = 0, out_2 = 0;

  if(*in1 < MD_NUM_LREGS+MD_NUM_LFREGS)
  {
    in_1 = RAT_t.alias[*in1]!=0?RAT_t.alias[*in1]:*in1;
    *in1 = in_1;
  }
  else
  {
    in_1 = (*in1 - (MD_NUM_LREGS+MD_NUM_LFREGS)) + MD_NUM_IREGS+MD_NUM_FREGS;;
    *in1 = in_1;
  }


  if(*in2 < MD_NUM_LREGS+MD_NUM_LFREGS)
  {
    in_2 = RAT_t.alias[*in2]!=0?RAT_t.alias[*in2]:*in2;
    *in2 = in_2;
  }
  else
  {
    in_2 = (*in2 - (MD_NUM_LREGS+MD_NUM_LFREGS)) + MD_NUM_IREGS+MD_NUM_FREGS;;
    *in2 = in_2;
  }

  if(*in3 < MD_NUM_LREGS+MD_NUM_LFREGS)
  {
    in_3 = RAT_t.alias[*in3]!=0?RAT_t.alias[*in3]:*in3; 
    *in3 = in_3;
  }
  else
  {
    in_3 = (*in3 - (MD_NUM_LREGS+MD_NUM_LFREGS)) + MD_NUM_IREGS+MD_NUM_FREGS;;
    *in3 = in_3;
  }

// Salida ******************************************************************************************
  if (*out1 >= (MD_NUM_LREGS+MD_NUM_LFREGS))
  {
    out_1 = (*out1 - (MD_NUM_LREGS+MD_NUM_LFREGS)) + MD_NUM_IREGS+MD_NUM_FREGS;
    out_2 = (*out2 - (MD_NUM_LREGS+MD_NUM_LFREGS)) + MD_NUM_IREGS+MD_NUM_FREGS;
    *out1 = out_1;
    *out2 = out_2;
  }
  if(*out1 >= MD_NUM_LREGS && *out1 < ((MD_NUM_LREGS+MD_NUM_LFREGS)-1))
  {
    int ind;
    int index;
    ind = FRL_FP_Get(&index);
    //printf("indice : %d\n", index);
    //printf("FRL_FP_Free : %d\tFRL_FP_Used : %d\n", FRL_FP_Free+128, FRL_FP_Used+128);
    if(ind==-1)printf("Se terminaron los registros : %d\t%d\n", FRL_FP_Free+128, FRL_FP_Used+128);
    else
    {
      RAT_t.alias[*out1] = index;
      out_1 = index;
    }

    *out1 = out_1;
    // out_1 = (*out1 - MD_NUM_LREGS) + MD_NUM_IREGS;
    // out_2 = (*out2 - MD_NUM_LREGS) + MD_NUM_IREGS;
    // *out1 = out_1;
    // *out2 = out_2;
  }
  if(*out1 != 0 && *out1 < ((MD_NUM_LREGS)))//+MD_NUM_LFREGS)-1))
  {
    int ind;
    int index;
    ind = FRLGet(&index);
    //printf("indice : %d\n", index);
    //printf("FRLFree : %d\tFRLUsed : %d\n", FRLFree, FRLUsed);
    if(ind==-1)printf("Se terminaron los registros : %d\t%d\n", FRLFree, FRLUsed);
    else
    {
      RAT_t.alias[*out1] = index;
      out_1 = index;
    }

    *out1 = out_1;
  }
  
  //printf("New Input dependencies: %d\t %d\t %d\n", *in1, *in2, *in3);
  //printf("New Output dependencies: %d\t %d\n", *out1, *out2);

  return answer;
}

int FRLPut(int new)
{
  if(new != 0 && new<((MD_NUM_IREGS)))//+MD_NUM_FREGS)-1))
  {
    if(FRLFree == FRLUsed)
    {
        return -1; /* FRL Full*/
    }

    FRLFree = (FRLFree + 1) % FRL_SIZE;

    return 0; // No errors
  }
  if(new >= MD_NUM_IREGS && new<((MD_NUM_IREGS+MD_NUM_FREGS)-1))
  {
    if(FRL_FP_Free == FRL_FP_Used)
    {
        return -1; /* FRL Full*/
    }
    FRL_FP_Free = (FRL_FP_Free + 1) % FRL_FP_SIZE;
    return 0;
  }
  return -1;
}

int FRLGet(int *old)
{
    if(FRLFree == ((FRLUsed + 1) % FRL_SIZE))
    {
        return -1; /* FRL Empty - nothing to get*/
    }

    *old = FRL[FRLUsed];

    FRLUsed = (FRLUsed + 1) % FRL_SIZE;

    return 0; // No errors
}

int FRL_FP_Get(int *old)
{
  if (FRL_FP_Free==((FRL_FP_Used+1) % FRL_FP_SIZE))
  {
    return -1;
  }
  *old = FRL[FRL_FP_Used + 127];
  FRL_FP_Used = (FRL_FP_Used + 1) % FRL_FP_SIZE;

  return 0;
}

int SRTPut()
{
  for (int i = 0; i < (MD_NUM_LREGS+MD_NUM_LFREGS); i++)
  {
    SRT_t[SRTIn].alias_names[i] = RAT_t.alias[i];
  }
 
  SRT_t[SRTIn].used = FRLUsed;
  SRT_t[SRTIn].FP_used = FRL_FP_Used;
  int id = SRTIn;
  SRTIn = (SRTIn + 1) % SRTSize;
  return id;
}

void SRTRecover(int index)
{
  for(int i=0; i<(MD_NUM_LREGS+MD_NUM_LFREGS); i++)
  {
    RAT_t.alias[i] = SRT_t[index].alias_names[i];
  }
  FRLUsed = SRT_t[index].used;
  FRL_FP_Used = SRT_t[index].FP_used;
}

// Julián Pavón Rivera *************************************************************************************


#if 0

/* floating point register file format */
union regs_FP_t {
    md_gpr_t l[MD_NUM_FREGS];			/* integer word view */
    md_SS_FLOAT_TYPE f[SS_NUM_REGS];		/* single-precision FP view */
    SS_DOUBLE_TYPE d[SS_NUM_REGS/2];		/* double-precision FP view */
};

/* floating point register file */
extern union md_regs_FP_t regs_F;

/* (signed) hi register, holds mult/div results */
extern SS_WORD_TYPE regs_HI;

/* (signed) lo register, holds mult/div results */
extern SS_WORD_TYPE regs_LO;

/* floating point condition codes */
extern int regs_FCC;

/* program counter */
extern SS_ADDR_TYPE regs_PC;

/* dump all architected register state values to output stream STREAM */
void
regs_dump(FILE *stream)		/* output stream */
{
  int i;

  /* stderr is the default output stream */
  if (!stream)
    stream = stderr;

  /* dump processor register state */
  fprintf(stream, "Processor state:\n");
  fprintf(stream, "    PC: 0x%08x\n", regs_PC);
  for (i=0; i<SS_NUM_REGS; i += 2)
    {
      fprintf(stream, "    R[%2d]: %12d/0x%08x",
	      i, regs_R[i], regs_R[i]);
      fprintf(stream, "  R[%2d]: %12d/0x%08x\n",
	      i+1, regs_R[i+1], regs_R[i+1]);
    }
  fprintf(stream, "    HI:      %10d/0x%08x  LO:      %10d/0x%08x\n",
	  regs_HI, regs_HI, regs_LO, regs_LO);
  for (i=0; i<SS_NUM_REGS; i += 2)
    {
      fprintf(stream, "    F[%2d]: %12d/0x%08x",
	      i, regs_F.l[i], regs_F.l[i]);
      fprintf(stream, "  F[%2d]: %12d/0x%08x\n",
	      i+1, regs_F.l[i+1], regs_F.l[i+1]);
    }
  fprintf(stream, "    FCC:                0x%08x\n", regs_FCC);
}

/* (signed) integer register file */
SS_WORD_TYPE regs_R[SS_NUM_REGS];

/* floating point register file */
union regs_FP regs_F;

/* (signed) hi register, holds mult/div results */
SS_WORD_TYPE regs_HI;
/* (signed) lo register, holds mult/div results */
SS_WORD_TYPE regs_LO;

/* floating point condition codes */
int regs_FCC;

/* program counter */
SS_ADDR_TYPE regs_PC;

#endif
